// Flutter code sample for material.DropdownButton.1

// This sample shows a `DropdownButton` whose value is one of
// "One", "Two", "Free", or "Four".

import 'package:flutter/material.dart';

class DropDown extends StatelessWidget {
  String dropdownValue;
  Function actualizar;
  Size media;

  List<DropdownMenuItem<String>> listDrop;
  List<String> prestamos = [
    'Prestamo Medico',
    'Prestamo de la Vivienda',
    'Prestamo para Vehiculos',
    'Prestamo Personales'
  ];

  String selected = '0';

  void loadData(Size media) {
    listDrop = [];

    for (var i = 0; i < prestamos.length; i++) {
      listDrop.add(DropdownMenuItem(
        child: Text(prestamos[i],style: TextStyle(fontSize: media.width * 0.039),),
        value: i.toString(),
      ));
    }

  }

  DropDown(this.selected, this.actualizar,this.media) {
    loadData(this.media);
  }

  @override
  Widget build(BuildContext context) {

var media = MediaQuery.of(context).size;

    return Center(
        child: Container(
          width:media.width,
          height: media.height * .10,
          alignment: Alignment.center,
          padding: EdgeInsets.only(right: media.width * 0.20,left:  media.width * 0.20),
      child: DropdownButton(
        items: listDrop,
        value: selected,
        onChanged: (value) {
           selected = value;
          actualizar(value);
        },
      ),
    ));
  }
}
