import 'package:calculadora/Logic/prestadora.dart';
import 'package:calculadora/screen/table.dart';
import 'package:calculadora/util/autoSize.dart';
import 'package:flutter/material.dart';
import '../widgets/dropDown.dart';

class Calculodora extends StatefulWidget {
  @override
  _CalculodoraState createState() => _CalculodoraState();
  String selected = '0';

  TextEditingController monto = TextEditingController();
  TextEditingController meses = TextEditingController();
}

class _CalculodoraState extends State<Calculodora> {
  @override
  Widget build(BuildContext context) {
    var media = MediaQuery.of(context).size;
    var orientation = MediaQuery.of(context).orientation;
    var textStyle = TextStyle(
        fontSize: media.width * .050,
        color: Colors.black38,
        fontWeight: FontWeight.normal);

    return Center(
      child: Container(
          width: media.width * 0.95,
          alignment: Alignment.topCenter,
          child: orientation == Orientation.portrait
              ? ListView(
                  children: <Widget>[
                    DropDown(widget.selected, actualizarDropDown, media),
                    SizedBox(height: 20),
                    myCustomTextField('Digite su monto', 'Monto Deseado',
                        Icon(Icons.attach_money), media, widget.monto),
                    SizedBox(height: 20),
                    myCustomTextField(
                        'Digite el plazo a pagar en meses',
                        'Plazo',
                        Icon(Icons.calendar_today),
                        media,
                        widget.meses),
                    SizedBox(height: 20),
                    RichText(
                      text: TextSpan(
                        text: 'Tasa Prefencial ',
                        style: textStyle,
                        children: <TextSpan>[
                          TextSpan(
                              text: Prestamos.tasaInteres.toString() + '%',
                              style: TextStyle(fontWeight: FontWeight.bold)),
                          TextSpan(text: ' Menual!'),
                        ],
                      ),
                    ),
                    SizedBox(height: 20),
                    Container(
                      height: media.height * 0.10,
                      width: 100,
                      child: MaterialButton(
                        onPressed: () {
                          if ((widget.meses.text.isEmpty ||
                              widget.monto.text.isEmpty)) {
                            _showDialog("Campos Inconrrectos",
                                "Favor Completar todos los campos de la forma correcta.");
                          } else if (double.tryParse(widget.meses.text) ==
                                  null ||
                              double.tryParse(widget.monto.text) == null) {
                            _showDialog("Campos Inconrrectos",
                                "Favor Completar todos los campos con valores numericos.");
                          } else {
                            setState(() {
                              Prestamos(double.parse(widget.monto.text),
                                  int.parse(widget.meses.text));
                            });
                          }
                        },
                        child: Text(
                          'Calcular',
                          style: TextStyle(
                              color: Colors.white, fontSize: media.width * .05),
                        ),
                      ),
                      color: Colors.red,
                    ),
                    SingleChildScrollView(
                      scrollDirection: Axis.vertical,
                      child: MyTable(Prestamos.pagosMensuales),
                    )
                  ],
                )
              : SingleChildScrollView(
                  scrollDirection: Axis.vertical,
                  child: MyTable(Prestamos.pagosMensuales),
                )),
    );
  }

  void actualizarDropDown(String value) {
    setState(() {
      widget.selected = value;
    });
  }

  void _showDialog(String title, String content) {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: new Text(title),
          content: new Text(content),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            new FlatButton(
              child: new Text("Cerrar"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  Widget myCustomTextField(String hintText, String labelText, Icon icon,
      Size media, TextEditingController controller) {
    var size = autoSize(media.width, media.height);
    var width = media.width * 0.030;

    return TextField(
      keyboardType: TextInputType.number,
      controller: controller,
      style: TextStyle(fontSize: width),
      decoration: new InputDecoration(
          hintText: hintText,
          hintStyle: TextStyle(fontSize: width),
          labelText: labelText,
          labelStyle: TextStyle(fontSize: width),
          suffixIcon: icon,
          border: OutlineInputBorder()),
    );
  }
}
