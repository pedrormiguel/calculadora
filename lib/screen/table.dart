import 'package:calculadora/Logic/pagos.dart';
import 'package:calculadora/util/autoSize.dart';
import 'package:flutter/material.dart';
import 'package:flutter/material.dart' as prefix0;
import 'package:intl/intl.dart';

class MyTable extends StatelessWidget {
  final List<Pagos> pagos;
  final oCcy = new NumberFormat("#,##0.00", "en_DOP");

  MyTable(this.pagos);

  @override
  Widget build(BuildContext context) {
    var media = MediaQuery.of(context).size;
    var width = media.width * 0.040;
    var widthColumn = media.width * 0.050;

    final List<DataColumn> columns = [
      DataColumn(
          label: Text(
        'Fecha',
        style: TextStyle(fontSize: widthColumn, fontWeight: FontWeight.bold),
      )),
      DataColumn(
          label: Text('Cuota',
              style: TextStyle(fontSize: widthColumn, fontWeight: FontWeight.bold))),
      DataColumn(
          label: Text('Balance Capital',
              style: TextStyle(fontSize: widthColumn, fontWeight: FontWeight.bold))),
      DataColumn(
          label: Text('Balance Interes',
              style: TextStyle(fontSize: widthColumn, fontWeight: FontWeight.bold))),
    ];

    return Container(
        color: Colors.blueAccent[100],
        padding: EdgeInsets.only(top: width * .90),
        alignment: Alignment.topCenter,
        child: Column(
          children: <Widget>[
            SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: DataTable(
                columnSpacing: width * .25,
                columns: columns,
                rows: pagos
                    .map((pago) => DataRow(cells: [
                          DataCell(Text(
                            pago.fecha,
                            
                            style: TextStyle(fontSize: width),
                          )),
                          DataCell(Text(
                              oCcy.format(double.parse(pago.cuotaCapital)),
                              style: TextStyle(fontSize: width))),
                          DataCell(Text(
                            oCcy.format(double.parse(pago.saldo)),
                            style: TextStyle(
                              fontSize: width,
                            ),
                          )),
                          DataCell(Text(
                              oCcy.format(double.parse(pago.sumaDeCuotas)),
                              style: TextStyle(fontSize: width)))
                        ]))
                    .toList(),
              ),
            )
          ],
        ));
  }
}
