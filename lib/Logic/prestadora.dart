import 'dart:math';
import 'package:calculadora/Logic/pagos.dart';

class Prestamos {
  final double montoPrestamo;
  double montoActualizado;
  double cuotaCapital;
  double abonoCapital;
  final int meses;
  static final double tasaInteres = 5 ;
  double interes;
  DateTime fechaDeSolicitud;
  DateTime fechaPagoProximo;
  double sumaDeCuotas = 0;
  bool ultimoPago = false;

  static List<Pagos> pagosMensuales = new List<Pagos>();

  double get getTasaInteres {
    return (tasaInteres / 12) / 100;
  }

  Prestamos(this.montoPrestamo, this.meses) {
    calcularCuota();
    this.interes = fixedNumers(this.montoPrestamo * getTasaInteres, 2);
    this.fechaDeSolicitud = DateTime.now();
    this.fechaPagoProximo = this.fechaDeSolicitud;
    this.montoActualizado = this.montoPrestamo;
    this.abonoCapital = fixedNumers(this.cuotaCapital - this.interes, 2);
    pagosMensuales.clear();
    calcular();
  }

  calcularCuota() {
    var potencia = pow((1 + getTasaInteres), meses);
    var numerador = potencia * getTasaInteres;
    var denominador = potencia - 1;

    this.cuotaCapital = double.parse(
        ((numerador / denominador) * montoPrestamo).toStringAsFixed(2));
  }

  String fechaDePagoCortada() {
    var fecha =
        '${this.fechaPagoProximo.day}/${this.fechaPagoProximo.month}/${this.fechaPagoProximo.year}';
    return fecha.toString();
  }

  void actualizarValores() {
    this.fechaPagoProximo = this.fechaPagoProximo.add(new Duration(days: 30));
    this.interes = fixedNumers(this.montoActualizado * getTasaInteres, 2);
    this.abonoCapital = fixedNumers(this.cuotaCapital - this.interes, 2);
    this.sumaDeCuotas = fixedNumers(this.sumaDeCuotas + this.cuotaCapital, 2);

    if ( (this.montoActualizado > this.abonoCapital))
      this.montoActualizado =
          fixedNumers(this.montoActualizado - this.abonoCapital, 2);
    else {
      this.abonoCapital = this.montoActualizado;
      this.ultimoPago = !this.ultimoPago;
    }
  }

  double fixedNumers(double number, int digit) {
    var output = number.toStringAsFixed(digit);

    return double.parse(output);
  }

  void calcular() {
    print(
        'Cuota Capital ${this.cuotaCapital} | Meses ${this.meses} | Fecha de Soliciud ${this.fechaDeSolicitud}');

    for (var i = 1; i <= meses; i++) {
      actualizarValores();
      print(
          '$i | Cuota Capital ${this.cuotaCapital} | Fecha ${fechaDePagoCortada()} | Abono Capital ${this.abonoCapital} | Interes ${this.interes}  |Saldo ${this.montoActualizado}');

      pagosMensuales.add(Pagos(
          numeroCuota: i.toString(),
          cuotaCapital: this.cuotaCapital.toString(),
          fecha: fechaDePagoCortada(),
          abonoCapital: this.abonoCapital.toString(),
          interes: this.interes.toString(),
          saldo: this.montoActualizado.toString(),
          sumaDeCuotas: this.sumaDeCuotas.toString()));
    }
  }
}
