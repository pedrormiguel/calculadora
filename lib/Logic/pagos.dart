class Pagos{
  final String numeroCuota;
  final String cuotaCapital;
  final String fecha;
  final String abonoCapital; 
  final String interes;
  final String sumaDeCuotas;
  final String saldo;

  Pagos({this.sumaDeCuotas,this.numeroCuota,this.cuotaCapital, this.fecha, this.abonoCapital, this.interes, this.saldo}); 
}